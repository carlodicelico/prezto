# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# set prompt
autoload -Uz promptinit
promptinit
prompt carlo

# ---------------------
# environment variables
# ---------------------

# local things
# Haskell for Mac
export PATH="/Users/cdicelico/.local/bin:$PATH"

# Mono framework
export MONO_GAC_PREFIX="/usr/local"
export PATH="$PATH:/Library/Frameworks/Mono.framework/Versions/Current/bin"

# Java/Android
export JAVA_HOME=$(/usr/libexec/java_home)
export ANDROID_HOME="/Users/carlodicelico/Library/Android/sdk"

# Julia
export PATH="$PATH:/Applications/JuliaPro-0.6.2.2.app/Contents/Resources/julia/Contents/Resources/julia/bin"

# for shell history in IEx
export ERL_AFLAGS="-kernel shell_history enabled"

# Postgres.app
export PATH="$PATH:/Applications/Postgres.app/Contents/Versions/9.6/bin"

# ssh
export SSH_KEY_PATH="~/.ssh"

# iTerm2 integration
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim' # 'mvim', 'emacs', etc.
fi

# init rbenv
eval "$(rbenv init -)"

# init nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# yarn
export PATH="`yarn global bin`:$PATH"

# homebrew
export PATH="/usr/local/Cellar:$PATH"

# asdf
source /usr/local/opt/asdf/asdf.sh


# ---------------------
# aliases
# ---------------------

# Force local vim
alias vim='/usr/local/bin/vim'

# Just for fun
alias bell='echo -ne "\007"'

# Push and pop directories on directory stack
alias pu='pushd'
alias po='popd'

# Basic directory operations
alias ...='cd ../..'
alias -- -='cd -'

# Super user
alias _='sudo'
alias please='sudo'

alias g='grep -in'

# Show history
alias history='fc -l 1'

# List directory contents
alias lsa='ls -lah'
alias l='ls -la'
alias ll='ls -l'
alias la='ls -lA'
alias sl=ls # often screw this up
alias lf='ls -lashF'
alias lt='ls -lashFt'

alias reload='source ~/.zshrc'

# recursively copy files in list $1 to dir $2, preserving dir structure
alias recp='cat $1 | xargs -I {} rsync -R {} $2'

# ---------------------
# functions
# ---------------------

# test truecolor support
function truecolor() {
    awk 'BEGIN{
        s="/\\/\\/\\/\\/\\"; s=s s s s s s s s s s s s s s s s s s s s s s s;
        for (colnum = 0; colnum<256; colnum++) {
            r = 255-(colnum*255/255);
            g = (colnum*510/255);
            b = (colnum*255/255);
            if (g>255) g = 510-g;
            printf "\033[48;2;%d;%d;%dm", r,g,b;
            printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
            printf "%s\033[0m", substr(s,colnum+1,1);
        }
        printf "\n";
    }' && tmux info | grep Tc
}

# cd into a dir and lf it
function cdlf() {
  cd $1; lf
}

# cd into a dir and lt it
function cdlt() {
  cd $1; lt
}

# make a directory and cd into it
function take() {
  mkdir -p $1
  cd $1
}

# OPAM configuration
. /Users/carlodicelico/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

###-tns-completion-start-###
if [ -f /Users/carlodicelico/.tnsrc ]; then
    source /Users/carlodicelico/.tnsrc
fi
###-tns-completion-end-###


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/carlodicelico/Downloads/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/carlodicelico/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/carlodicelico/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/carlodicelico/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
